<?php

namespace Drupal\git_tag\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class GitTagSettingsForm.
 */
class GitTagSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'git_tag.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'git_tag_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('git_tag.settings');

    $form['json'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Json file'),
      '#description' => $this->t('Enter location where to retrieve the json file.'),
      '#default_value' => $config->get('json'),
      '#required' => TRUE,
      '#maxlength' => 64,
      '#size' => 64,
      '#weight' => '0',
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    if (!file_exists($form_state->getValue('json'))) {
      $form_state->setErrorByName('json', $this->t("Your file doesn't  exist."));
    }

    $json = json_decode(file_get_contents($form_state->getValue('json')), true);
    if (NULL === $json) {
      $form_state->setErrorByName('json', $this->t('Your file need to be a valid json file.'));
    }

    if (!isset($json['tag'])) {
      $form_state->setErrorByName('json', $this->t('Your file is not correctly formatted.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->messenger()->addMessage($this->t('Your json file @file is configured.', [
      '@file' => $form_state->getValue('json'),
    ]));

    $this->config('git_tag.settings')
      ->set('json', $form_state->getValue('json'))
      ->save();
  }

}
